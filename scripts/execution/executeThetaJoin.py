import os.path;
import subprocess;
import sys;
import os;

jarFilePath = sys.argv[1];
parametersFilePath = sys.argv[2];
environment = sys.argv[3];

directoryAbsolutePath = os.path.dirname(os.path.realpath(filePath));

fileName = os.path.basename(filePath);
fileNameWithoutExt, fileExtension = os.path.splitext(fileName);
fileExtension = fileExtension.translate(None,'.');

fileNameCompleted = directoryAbsolutePath + "/" + "completed_" + fileName;
fileNameStderrout = directoryAbsolutePath + "/" + "stderrout_" + fileName;

with open(filePath) as f:
	content = f.readlines();
	contentParams = [tmpSTR.replace("\n","") for tmpSTR in content];
	
contentParamsCompleted = [];
if os.path.isfile(fileNameCompleted):
	with open(fileNameCompleted) as f:
		content = f.readlines();
		contentParamsCompleted = [tmpSTR.replace("\n","") for tmpSTR in content];

print contentParamsCompleted;

if os.path.isfile(fileNameCompleted):
	fCompleted = open(fileNameCompleted,'a');
else:
	fCompleted = open(fileNameCompleted,'w');

fStderrout = open(fileNameStderrout,'w');

for params in contentParams:
	
	if params in contentParamsCompleted:
		continue;
	
	print("executing: " + params + "\n");
	fStderrout.write("executing: " + params + "\n");
	fStderrout.flush();
	
	executionParams = "";
	if environment == "hadoop":
		executionParams = "hadoop jar " + jarFilePath + " " + params;
	elif environment == "spark": # https://spark.apache.org/docs/1.2.1/submitting-applications.html
		environmentMode = sys.argv[4];
		cores = sys.argv[5];
		
		executionParams = "spark-submit ";
		executionParams += "--class binarythetajoins.btj.ThetaJoin "
		
		if environmentMode == "local":
			executionParams += "--master local[" + cores + "] ";
		elif environmentMode == "master":
			executionParams += "--master spark://155.207.131.28:7077 ";
			executionParams += "--supervise "; # make sure that the driver is automatically restarted if it fails with non-zero exit code
			executionParams += "--executor-memory 10G ";
			executionParams += "--total-executor-cores " + cores + " ";
		else:
			print("Error no environmentMode specified ! Exiting!");
			exit(-1);
		executionParams += jarFilePath" ";
		executionParams += params;
	
	p = subprocess.Popen(executionParams,bufsize=1024,shell=True,stdout=fStderrout,stderr=subprocess.STDOUT);#stdout=subprocess.PIPE,stderr=subprocess.PIPE);
	p.wait();
	fCompleted.write(params);
	fCompleted.flush();
	
fStderrout.close();
fCompleted.close();
