import os.path;
import subprocess;
import sys;
import os;

filePath = sys.argv[1];

directoryAbsolutePath = os.path.dirname(os.path.realpath(filePath));

fileName = os.path.basename(filePath);
fileNameWithoutExt, fileExtension = os.path.splitext(fileName);
fileExtension = fileExtension.translate(None,'.');

fileNameCompleted = directoryAbsolutePath + "/" + "completed_" + fileName;
fileNameStderrout = directoryAbsolutePath + "/" + "stderrout_" + fileName;

with open(filePath) as f:
	content = f.readlines();
	contentParams = [tmpSTR.replace("\n","") for tmpSTR in content];
	
contentParamsCompleted = [];
if os.path.isfile(fileNameCompleted):
	with open(fileNameCompleted) as f:
		content = f.readlines();
		contentParamsCompleted = [tmpSTR.replace("\n","") for tmpSTR in content];

print contentParamsCompleted;

if os.path.isfile(fileNameCompleted):
	fCompleted = open(fileNameCompleted,'a');
else:
	fCompleted = open(fileNameCompleted,'w');

fStderrout = open(fileNameStderrout,'w');

for params in contentParams:
	
	if params in contentParamsCompleted:
		continue;
	
	print("executing: " + params + "\n");
	fStderrout.write("executing: " + params + "\n");
	fStderrout.flush();
	
	executionParams = "hadoop jar btj_controller.jar " + params;
	
	p = subprocess.Popen(executionParams,bufsize=1024,shell=True,stdout=fStderrout,stderr=subprocess.STDOUT);#stdout=subprocess.PIPE,stderr=subprocess.PIPE);
	p.wait();
	fCompleted.write(params);
	fCompleted.flush();
	
fStderrout.close();
fCompleted.close();
