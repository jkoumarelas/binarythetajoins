import sys;
import os;
import paramiko;
import time;
import os.path;
import random;

############################################
#####				Main				####
def main():
	# Create the nodes dictionary object
	nodes = getNodes(); #printNodes(nodes);

	# send, stop, reset, receive
	# TODO: stop, reset
	mode = sys.argv[1];
	
	
	#localFilePath = "parameters/cloudSolarAltitude19/solarAltitude19/rearrangements.txt";
	#localFilePath = "parameters/cloudSolarAltitude19/solarAltitude19/partitioning.txt";
	
	if mode == "send":
		localFilePath = sys.argv[2];
		sendExecutions(localFilePath, nodes);
	elif mode == "stop":
		stopExecution(nodes);
	elif mode == "reset":
		resetNodes(nodes);
	elif mode == "receive":
		localFilePath = sys.argv[2];
		receiveResults(localFilePath, nodes);
	else:
		print "*** WRONG MODE GIVEN ***";
############################################
#####		Global Variables			####

# FileNames
completedFileName = "completed.txt";
pidFileName = "run.pid";
nohupFileName = "nohup.out";

# Paths
# http://stackoverflow.com/questions/3430372/how-to-get-full-path-of-current-files-directory-in-python
# directory of the script being run
localBaseAbs = os.path.dirname(os.path.abspath(__file__));
remoteBaseAbs = "/root/users/koumarelas/btj/workspace";

paramsRel = "parameters";
rsyncRel = "datasets";

# MASTER rsync
master = "rabbit";
###########################################
# Stop execution
def stopExecution(nodes):
	for hostname, infos in sorted(nodes.items()):
		host = infos['ip'];
		username = infos['username'];
		
		cmdToExecute = "cd " + remoteBaseAbs + ";";
		cmdToExecute += "cat " + pidFileName;
		
		linesOUT, linesERR = executeCommandAtHost(cmdToExecute,host,username);
		
		pid = int(linesOUT[0].encode('utf-8').translate(None,'\n'));
		
		#cmdToExecute = "kill -9 " + str(pid);
		cmdToExecute = "pkill -9 -f btj_producer.jar";
		
		linesOUT, linesERR = executeCommandAtHost(cmdToExecute,host,username);
		
		print "hostname: " + hostname + " pid: " + str(pid) + " terminated";
###########################################
# Reset execution
def resetNodes(nodes):
	masterUsername = nodes[master]['username'];
	masterHostname = nodes[master]['ip'];
	
	pathSync = remoteBaseAbs + "/" + rsyncRel;
	
	excludePattern = "--exclude={*.png,*.eps}";
	
	print "reset rsync LOCAL --> MASTER";
	# MASTER --> LOCAL
	masterPathSync = masterUsername + "@" + masterHostname + ":" + pathSync;
	
	localPathSync = localBaseAbs + "/" + rsyncRel;

	cmdToExecute = "rsync -az --delete --force --delete-excluded " + excludePattern + " " + localPathSync + "/ " + masterPathSync;
	
	print cmdToExecute;
	os.system(cmdToExecute);
	
	# MASTER --> SLAVES
	print "reset rsync MASTER --> SLAVES";
	for hostname, infos in sorted(nodes.items()):
		if hostname == master:
			continue;
		
		slaveUsername = infos['username'];
		slaveHostname = infos['ip'];
		
		slavePathSync = slaveUsername + "@" + slaveHostname + ":" + pathSync;
		
		cmdToExecute = "ssh " + masterUsername + "@" + masterHostname + " ";
		cmdToExecute += "\'";
		cmdToExecute += "rsync -az --delete --force --delete-excluded " + excludePattern + " " + pathSync + "/ " + slavePathSync;
		cmdToExecute += "\'";
		
		print cmdToExecute;
		os.system(cmdToExecute);

###########################################
# Receive results
def receiveResults(localFilePath, nodes):
	allProcessesFinished = True;
	
	# http://stackoverflow.com/questions/20254155/how-to-run-nohup-and-write-its-pid-file-in-a-single-bash-statement
	for hostname, infos in sorted(nodes.items()):
		host = infos['ip'];
		username = infos['username'];
		
		cmdToExecute = "cd " + remoteBaseAbs + ";";
		cmdToExecute += "cat " + pidFileName;
		
		linesOUT, linesERR = executeCommandAtHost(cmdToExecute,host,username);
		
		pid = int(linesOUT[0].encode('utf-8').translate(None,'\n'));
		
		cmdToExecute = "kill -0 " + str(pid);
		
		linesOUT, linesERR = executeCommandAtHost(cmdToExecute,host,username);
		
		processFinished = len(linesERR)>0;
		
		infos['processFinished'] = processFinished;
		
		allProcessesFinished = allProcessesFinished and processFinished;
		
		print "hostname: " + hostname + " pid: " + str(pid) + " processFinished: " + str(processFinished);
	
	while allProcessesFinished == False:
		time.sleep(30);
		print "";
		
		for hostname, infos in sorted(nodes.items()):
			
			if infos['processFinished']:
				continue;
			
			host = infos['ip'];
			username = infos['username'];
			
			localFileDirAbs = os.path.dirname(os.path.realpath(localFilePath));
			
			# http://stackoverflow.com/questions/1192978/python-get-relative-path-of-all-files-and-subfolders-in-a-directory
			# get relative path:
			localFileDirRel = os.path.relpath(localFileDirAbs, localBaseAbs);
			fileName = os.path.basename(localFilePath);
			#fileNameWithoutExt, fileExtension = os.path.splitext(fileName);
			#fileExtension = fileExtension.translate(None,'.');
			
			datasetParamsDirRel = localFileDirRel;
			
			#grep -F -x -v -f nohup.out parameters/cloudSolarAltitude19/solarAltitude19/partitioning.txt | wc -l
			
			completedParams = remoteBaseAbs + "/" + datasetParamsDirRel + "/" + "completed_" + fileName;
			totalParams = remoteBaseAbs + "/" + datasetParamsDirRel + "/" + fileName;
			
			cmdToExecute = "grep -F -x -v -f " + completedParams + " " + totalParams + " | wc -l";
			linesOUT, linesERR = executeCommandAtHost(cmdToExecute,host,username);
			
			remainingLines = int(linesOUT[0].encode('utf-8').translate(None,'\n'));
			
			cmdToExecute = "cat " + totalParams + " | wc -l";
			
			linesOUT, linesERR = executeCommandAtHost(cmdToExecute,host,username);
			
			totalLines = int(linesOUT[0].encode('utf-8').translate(None,'\n'))
			
			completedLines = totalLines - remainingLines;
			
			completion = float(completedLines) / totalLines
			
			cmdToExecute = "hostname: " + hostname + " completion: " + str(completion) + "(" + str(completedLines) + "/" + str(totalLines) + ")";
			print cmdToExecute;
			
			processFinished = (completedLines == totalLines);
			allProcessesFinished = allProcessesFinished and processFinished;
			
			infos['processFinished'] = processFinished;
		
	if allProcessesFinished:
		excludePattern = "--exclude={*.png,*.eps}"
		
		masterUsername = nodes[master]['username'];
		masterHostname = nodes[master]['ip'];
		
		pathSync = remoteBaseAbs + "/" + rsyncRel;
		
		# Delete files prior to anything else
		for hostname, infos in sorted(nodes.items()):
			host = infos['ip'];
			username = infos['username'];
			
			cmdToExecute = "find "+remoteBaseAbs + "/" + rsyncRel +" -name \"*.crc\" -type f -delete";
			
			executeCommandAtHost(cmdToExecute, host, username);
			
		print "SLAVES --> MASTER";
		
		# SLAVES --> MASTER
		for hostname, infos in sorted(nodes.items()):
			if hostname == master:
				continue;
			
			slaveUsername = infos['username'];
			slaveHostname = infos['ip'];
			
			slavePathSync = slaveUsername + "@" + slaveHostname + ":" + pathSync;
			
			cmdToExecute = "ssh " + masterUsername + "@" + masterHostname + " ";
			cmdToExecute += "\'";
			cmdToExecute += "rsync -az " + excludePattern + " " + slavePathSync + "/ " + pathSync;
			cmdToExecute += "\'";
			
			print cmdToExecute;
			os.system(cmdToExecute);
		
		print "MASTER --> SLAVES";
		
		# MASTER --> SLAVES
		for hostname, infos in sorted(nodes.items()):
			if hostname == master:
				continue;
			
			slaveUsername = infos['username'];
			slaveHostname = infos['ip'];
			
			slavePathSync = slaveUsername + "@" + slaveHostname + ":" + pathSync;
			
			cmdToExecute = "ssh " + masterUsername + "@" + masterHostname + " ";
			cmdToExecute += "\'";
			cmdToExecute += "rsync -az " + excludePattern + " " + pathSync + "/ " + slavePathSync;
			cmdToExecute += "\'";
			
			print cmdToExecute;
			os.system(cmdToExecute);
			
		print "MASTER --> LOCAL";
		# MASTER --> LOCAL
		masterPathSync = masterUsername + "@" + masterHostname + ":" + pathSync;
		
		localPathSync = localBaseAbs + "/" + rsyncRel;

		cmdToExecute = "rsync -az " + excludePattern + " " + masterPathSync + "/ " + localPathSync;
		
		print cmdToExecute;
		os.system(cmdToExecute);
###########################################
# Send executions
def sendExecutions(localFilePath, nodes):
	localFileDirAbs = os.path.dirname(os.path.realpath(localFilePath));
	
	# http://stackoverflow.com/questions/1192978/python-get-relative-path-of-all-files-and-subfolders-in-a-directory
	# get relative path:
	localFileDirRel = os.path.relpath(localFileDirAbs, localBaseAbs);
	fileName = os.path.basename(localFilePath);
	fileNameWithoutExt, fileExtension = os.path.splitext(fileName);
	fileExtension = fileExtension.translate(None,'.');

	#for local, value in locals().iteritems():
	#	print local + "\t" + str(value);
	#exit(0);

	# Read file's content
	with open(localFilePath, 'r') as fin:
		content = fin.readlines();
		
	lines = len(content);
	linesMatchedSum = 0;

	indicesShuffled = range(0,lines);
	random.shuffle(indicesShuffled);

	####################################################################
	# Split file according to nodes' processing power
	for i, hostname in enumerate(sorted(nodes.keys())):
		infos = nodes[hostname];
		
		# Every node except the last
		if(i+1 < len(nodes.keys())):
			linesMatched = int(lines * float(infos['processingPowerShare']));
		else: # The last node will have to receive the remaining lines
			linesMatched = lines - linesMatchedSum;

		nodeFileName = fileNameWithoutExt + "_" + hostname + "." + fileExtension;
		
		infos['nodeFileName'] = nodeFileName;
		
		with open(localFileDirRel + "/" + nodeFileName, 'w') as fout:
			for counter in range(0, linesMatched):
				#print str(counter) + "\t" + str(linesMatchedSum);
				fout.write(content[indicesShuffled[counter + linesMatchedSum]]);
		
		linesMatchedSum += linesMatched;
		print "hostname: " + hostname + "\t" + "linesMatched: " + str(linesMatched);
	
	####################################################################
	# Send each file to the corresponding node and execute a command.
	for hostname, infos in sorted(nodes.items()):
		
		datasetParamsDirRel = localFileDirRel;
		
		localPath = datasetParamsDirRel + "/" + infos['nodeFileName'];
		remotePath = remoteBaseAbs + "/" + datasetParamsDirRel + "/" + fileName;
		
		completedParams =  localFileDirRel + "/" + "completed_" + fileName;
		#print "localPath: " + localPath + "\t" + "remotePath: " + remotePath;
		
		host = infos['ip'];
		username = infos['username'];
		putFlag = True; # send file from local to remote
		
		## First create the parameters folder if it does not exist.
		# -p: recursive
		cmdToExecute = "rm -r " + remoteBaseAbs + "/" + datasetParamsDirRel;
		cmdToExecute += "mkdir -p " + remoteBaseAbs + "/" + datasetParamsDirRel;
		#print "cmdToExecute: " + cmdToExecute;
		executeCommandAtHost(cmdToExecute, host, username);

		# And then send the file
		transferFile(localPath,remotePath,host,username,putFlag);
		
		# commands
		redirectSTDERRtoSTDOUT_cmd = "2>&1";
		exportPIDtoFile_cmd = "echo $! > " + pidFileName; #exportPIDtoFile_cmd = " 2>&1 & echo $! > run.pid";
		exportFinishedFile_cmd = "touch "+completedFileName;
		
		changeDirCmd = "cd " + remoteBaseAbs;
		removeFilesAndDirs = [completedFileName, completedParams, pidFileName, nohupFileName];
		
		prior_cmd = changeDirCmd + ";" + "rm -r " + " ".join(removeFilesAndDirs);
		
		##################################
		#*** THIS IS THE MAIN COMMAND ***#
		threadsNum = infos['cores'];
		threadsSleepTime = '3'; # in ms
		maxJavaHeapMB = (int(infos['ram']) -2) * 1024;
		
		main_cmd = "python2 executeLocalParallel.py " + localFilePath;#datasetParamsDirRel + ;
		main_cmd += " " + threadsNum + " " + threadsSleepTime + " " + str(maxJavaHeapMB);
		
		#main_cmd = "sleep 2 ; mkdir -p " + rsyncRel + " ; touch " + rsyncRel + "/" + username + "_" + host;
		#main_cmd = "sleep 200";
		##################################
		
		after_cmd = exportFinishedFile_cmd;
		
		# http://serverfault.com/questions/373052/is-there-a-difference-between-how-two-ampersands-and-a-semi-colon-operate-in-bas
		# &:	directs the shell to run the command in the background, that is, it is forked and run in a separate sub-shell, as a 
		#		job, asynchronouslyhttp://bashitout.com/2013/05/18/Ampersands-on-the-command-line.html
		# a && b:	if a returns zero exit code, then b is executed.
		# a || b:	if a returns non-zero exit code, then b is executed.
		# a ; b	:	a is executed and then b is executed.
		cmdToExecute = prior_cmd  + "; "
		##cmdToExecute += "nohup sh -c \'" + main_cmd + " && " + after_cmd;
		cmdToExecute += "nohup sh -c \'" + main_cmd + " ; " + after_cmd;
		cmdToExecute += "\' > " + nohupFileName;
		cmdToExecute += " " + redirectSTDERRtoSTDOUT_cmd + " & " + exportPIDtoFile_cmd;
		print cmdToExecute + "\n";
		
		executeCommandAtHost(cmdToExecute,host,username);

########################################################################
########################################################################
# http://stackoverflow.com/questions/68335/how-do-i-copy-a-file-to-a-remote-server-in-python-using-scp-or-ssh
# Send file to host
#def transferFile(localFileName, remoteFileName, localParamsDirPath, remoteParamsDirPath, nodeHost, nodeUsername): #localFilePath, remoteDirectoryPath, remoteFilePath
def transferFile(localPath, remotePath, nodeHost, nodeUsername, putFlag): # putFlag: if true, put from local to remote, else if false, put from remote to local
	ssh = paramiko.SSHClient();
	
	## Option 1 - with key file
	## http://stackoverflow.com/questions/18362051/how-to-connect-to-remote-server-with-paramiko-without-a-password
	privateKeyFile = os.path.expanduser('~/.ssh/id_rsa');
	nodeKey = paramiko.RSAKey.from_private_key_file(privateKeyFile);
	
	ssh.load_host_keys(os.path.expanduser(os.path.join("~", ".ssh", "known_hosts")));
	
	ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy());
	
	ssh.connect(nodeHost, username = nodeUsername, pkey = nodeKey);
	#ssh.connect(nodeHost, username="USERNAME", password="PASSWORD");
	
	sftp = ssh.open_sftp();
	
	# Create directory to send file into
	#try:
	#	sftp.chdir(remoteParamsDirPath)  # Test if remoteDirectoryPath exists
	#except IOError:
	#	sftp.mkdir(remoteParamsDirPath)  # Create remoteDirectoryPath
	#	sftp.chdir(remoteParamsDirPath)
	
	if putFlag:
		sftp.put(localPath, remotePath);
	else:
		sftp.get(remotePath, localPath);

	sftp.close();
	ssh.close();
########################################################################
# Execute command at host
def executeCommandAtHost(cmdToExecute, nodeHost, nodeUsername):
	ssh = paramiko.SSHClient();
	privatekeyfile = os.path.expanduser('~/.ssh/id_rsa');
	nodeKey = paramiko.RSAKey.from_private_key_file(privatekeyfile);
	
	ssh.load_host_keys(os.path.expanduser(os.path.join("~", ".ssh", "known_hosts")));
	
	ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy());
	
	ssh.connect(nodeHost, username = nodeUsername, pkey = nodeKey);
	#ssh.connect(nodeHost, username="USERNAME", password="PASSWORD");
	
	ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(cmdToExecute);
	
	#linesOUT = ssh_stdout.read.splitlines();
	#linesERR = ssh_stderr.read.splitlines();
	linesOUT = ssh_stdout.readlines();
	linesERR = ssh_stderr.readlines();
	
	ssh.close();
	
	return linesOUT, linesERR;
########################################################################
# Print nodes
def printNodes(nodes):
	for hostname, infos in sorted(nodes.items()):
		print hostname + "\t";
		for info, value in sorted(infos.items()):
			print info + "\t" + value + "\t";
		print "\n";
########################################################################
# Get nodes object that contains information for each node.
def getNodes():
	nodes = {
	"rabbit" : {
		#"ip" : "155.207.131.28",
		"ip" : "rabbit.csd.auth.gr",
		"cores" : "16",
		"ram" : "64", # GB
		"power" : "0.9",
		"username" : "root"},
	"panther" : {
		#"ip" : "155.207.131.13",
		"ip" : "panther.csd.auth.gr",
		"cores" : "8",
		"ram" : "32",
		"power" : "1.2",
		"username" : "root"},
	"anaconda" : {
		#"ip" : "155.207.131.15",
		"ip" : "anaconda.csd.auth.gr",
		"cores" : "12",
		"ram" : "64",
		"power" : "0.85",
		"username" : "root"}#,
	#"deer" : {
	#	#"ip" : "155.207.131.66",
	#	"ip" : "deer.csd.auth.gr",
	#	"cores" : "8",
	#	"ram" : "32",
	#	"power" : "0.7",
	#	"username" : "root"}
	}
	totalProcessingPower = 0.0;

	# Calculate the total processing power
	for node, infos in nodes.items():
		#for info, value in infos.items():
		cores = int(infos['cores']);
		power = float(infos['power']);
		totalProcessingPower += cores * power;

	# Now calculate the processing power of each node
	for node, infos in nodes.items():
		cores = int(infos['cores']);
		power = float(infos['power']);
		processingPower = cores * power;
		processingPowerShare = processingPower / totalProcessingPower;
		
		infos['processingPower'] = str(processingPower);
		infos['processingPowerShare'] = str(processingPowerShare);
	
	return nodes;

########################################################################
########################################################################
if __name__ == "__main__":
	main();
