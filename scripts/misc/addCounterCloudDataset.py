from os import walk
import os.path
import re #regular expressions
import random

def main():
	inputPath = "/home/koumarel/datasets/cloudDatasetCompressed/";
	outputPath = inputPath;#"./cloudDatasetOutput";
	
	inputFiles = []
	for (dirpath, dirnames, filenames) in walk(inputPath):
		inputFiles.extend(filenames)
		break
	
	print "inputFiles";
	print inputFiles;
	
	counter = 0L;
	
	for inputFile in inputFiles:
		print inputFile + "\n";
		
		fin = open(inputPath + "/" + inputFile, 'r');
		outputFilePath = outputPath + "/" + inputFile + "_"+ "new" + ".csv";
		fout = open(outputFilePath, 'w');
		for line in fin:
			toks = line.split(",");
			
			outStr = str(counter) + "," + toks[0] + "," + toks[1] + "\n";
			#print outStr;
			fout.write(outStr);
			counter += 1;
		
		fin.close();
		fout.close();
		
		outputFilePathTmp = outputFilePath + "_tmp";
		os.rename(outputFilePath,outputFilePathTmp);
		
		# SHUFFLE LINES
		# Then open the temporary file, open a new (final) file, shuffle it (the temporary file)
		# and only keep the real output file (delete the temp file).
		with open(outputFilePathTmp,'r') as source:
			data = [ (random.random(),line) for line in source]
			data.sort()
			with open(outputFilePath,'w') as target:
				for _, line in data:
					target.write(line);

		os.remove(outputFilePathTmp);

if __name__ == "__main__":
	main()
