import matplotlib.pyplot as plt
import csv, sys, os
import collections
from matplotlib.pyplot import text;
from matplotlib.font_manager import FontProperties
from matplotlib.patches import Rectangle
import pylab;

def getCellToPartitionID(p2cFilePath):
	c2p = dict();
	with open(p2cFilePath, 'rb') as f:
		for i, row in enumerate(f):
			# remove characters to just split with the space character.
			str1 = row.replace(',',' ');
			str2 = str1.replace('[',' ');
			str3 = str2.replace(']',' ');
			toks = str3.split();#row.split(', | ');
			partitionID = toks[0];
			for ni in xrange(1,len(toks),2):
				c2p[toks[ni] + "_" + toks[ni+1]] = partitionID;
	return c2p;
	
def getPartitionIDToBoundaries(p2bndsFilePath):
	p2bnds = dict();
	with open(p2bndsFilePath, 'rb') as f:
		for i, row in enumerate(f):
			# remove characters to just split with the space character.
			str1 = row.replace(',',' ');
			str2 = str1.replace('[',' ');
			str3 = str2.replace(']',' ');
			toks = str3.split();#row.split(', | ');
			partitionID = toks[0];
			
			upperLeftRow = int(toks[1]);
			upperLeftColumn = int(toks[2]);
			
			lowerLeftRow = int(toks[3]);
			lowerLeftColumn = int(toks[4]);
			
			upperRightRow = int(toks[5]);
			upperRightColumn = int(toks[6]);
			
			lowerRightRow = int(toks[7]);
			lowerRightColumn = int(toks[8]);
			
			x0 = upperLeftRow;
			y0 = upperLeftColumn;
			
			width = int(upperRightColumn - upperLeftColumn + 1);
			height = int(lowerLeftRow - upperLeftRow + 1);
			
			#print(locals());
			
			p2bnds[partitionID] = {'x0':x0,'y0':y0,'width':width,'height':height};
			
	return p2bnds;

pmFilePath = None;
p2cFilePath = None;
p2bndsFilePath = None;
pmEPSFilePath = "pm.eps";

plotDots = True;

if len(sys.argv) == 1:
	pmFilePath = 'pm.csv';
	pmEPSFilePath = "pm.eps";
	p2cFilePath = 'partitionToCellsMapping.csv';
	p2bndsFilePath = "partitionRectangularBoundaries.csv";
else :
	pmFilePath = sys.argv[1];
	pmEPSFilePath = sys.argv[2];
	if len(sys.argv) >= 4:
		pointsColor = 'w';
		p2cFilePath = sys.argv[3];
		plotDots = False;
		if len(sys.argv) >= 5:
			plotDots = True;
			if sys.argv[4] == "dots":
				p2bndsFilePath = "";
			else:
				p2bndsFilePath = sys.argv[4];
	else:
		pointsColor = 'k'; #'r','k','w';

if p2cFilePath is not "" and p2cFilePath is not None:
	c2p = getCellToPartitionID(p2cFilePath);

if p2bndsFilePath is not "" and p2bndsFilePath is not None:
	p2bnds = getPartitionIDToBoundaries(p2bndsFilePath);

idxS = [];
idxT = [];

with open(pmFilePath, 'rb') as csvfile:
	reader = csv.reader(csvfile, delimiter=',');#, quotechar='|')
	for i, row in enumerate(reader):
		for j, cell in enumerate(row):
			if cell != "0" and cell != "":
				idxS.append(i);
				idxT.append(j);

fig = plt.figure(figsize=(7.0, 7.0)); # 7 inches

pylab.ylim([0,100]);
pylab.xlim([0,100]);

# http://stackoverflow.com/questions/3584805/in-matplotlib-what-does-111-means-in-fig-add-subplot111
ax1 = fig.add_subplot(1, 1, 1); # 1x1 grid, first subplot
ln = ax1.plot(idxT,idxS, '.', color=pointsColor); # markers: http://matplotlib.org/api/markers_api.html

ax1.set_xlabel('T');
ax1.set_ylabel('S',rotation='horizontal');

ax1.xaxis.tick_top();
ax1.xaxis.set_label_position('top');
#plt.title("JM");
figure_title = "";#"Join Matrix";
plt.text(0.5, -0.07, figure_title,
	horizontalalignment='center',
	transform = ax1.transAxes);
plt.gca().invert_yaxis();

if p2cFilePath is not "" and p2cFilePath is not None:
	if (p2bndsFilePath is not "" and p2bndsFilePath is not None) or plotDots == True:
		ln = ax1.plot(idxT,idxS, 'r.');
	else:
		# Add points on the plot, with white color (you cannot see them in white background) and as an annotation their label, which is black and can be seen.
		for i, idx in enumerate(idxS):
			#ax1.annotate(c2p[str(idxS[i]) + '_' + str(idxT[i])], xy=(idxT[i],idxS[i]), xytext = (0,0), textcoords='offset points', size=14);# default ~10?
			ax1.annotate(c2p[str(idxS[i]) + '_' + str(idxT[i])], xy=(idxT[i],idxS[i]), xytext = (-2,-7), textcoords='offset points', size=20);# default ~10?
			
	# Rectangles and their reducerID are drawn only if this is a rectangular partitioner.
	if p2bndsFilePath is not "" and p2bndsFilePath is not None:
		for k in p2bnds.keys():
			partitionID = k;
			values = p2bnds[k];
			rect = Rectangle((values['y0'],values['x0']), values['width'], values['height'], ec="black", fc="none", zorder=10, alpha=0.1, visible=True);
			ax1.add_patch(rect);
			plt.text(values['y0'] + values['width']/2, values['x0'] + values['height']/2,reducerID,ha='center',va="center",fontsize=50,weight="bold");

plt.draw();
fig.savefig(pmEPSFilePath);
plt.show();
