import csv
import sys
from statistics import mean
from statistics import median
from statistics import stdev
import pandas
import numpy
from pandas.core.frame import DataFrame
import csv
from functools import cmp_to_key


metrics = []
metrics_idx = []
operators = []

_mpi = "BJmaxIC"
_mpcl = "BJmaxCC"
_rep = "BJrepIC"
_imb = "BJimbIC"

_nparts = "numPartitionsPartitioning"
_npartsu = "numPartitionsUsedPartitioning"


def get_diffs_operator_metric(diffs_cases, metric_index, operator, diffs_stddev):
    result = [];
    if operator == "<<":
        result = [diff_case for diff_case in diffs_cases if diff_case[metric_index] <= (-diffs_stddev)]
    elif operator == "<":
        result = [diff_case for diff_case in diffs_cases if (-diffs_stddev) < diff_case[metric_index] <= -0.1 *
                  diffs_stddev]
    elif operator == "==":
        result = [diff_case for diff_case in diffs_cases if abs(diff_case[metric_index]) < 0.1 * diffs_stddev]
    elif operator == ">":
        result = [diff_case for diff_case in diffs_cases if (0.1 * diffs_stddev) <= diff_case[metric_index] <
                  diffs_stddev]
    elif operator == '>>':
        result = [diff_case for diff_case in diffs_cases if (diffs_stddev <= diff_case[metric_index])]
    elif operator is None:
        result = []

    return result


def get_cases_given_metrics_operands(diffs_cases, diffs_stddev):
    global metrics, metrics_idx, operators

    cases_set = set([])

    first_metric = True

    for i in range(0, 4):
        m_i = metrics[i]
        o_i = operators[m_i]
        if o_i is not None:
            m_i_diffs_operator = get_diffs_operator_metric(diffs_cases, metrics_idx[m_i], o_i, diffs_stddev[m_i])
            if first_metric:
                cases_set = set(m_i_diffs_operator)
                first_metric = False
            else:
                cases_set = cases_set & set(m_i_diffs_operator)

    return cases_set


def get_metrics_operator_diff(a_val, b_val, op):
    qnt = 0
    if op == '==':
        qnt = abs(abs(a_val) - abs(b_val))
    elif op == '>' or op == '>>':
        qnt = (a_val - b_val) * -1
    elif op == '<' or op == '<<':
        qnt = a_val - b_val
    elif op is None:
        qnt = 0

    return 1 if qnt > 0 else -1


def cmp_cases(a, b):
    global metrics, metrics_idx, operators
    qnt = 0
    for i in range(0, len(metrics)):
        m_i = metrics[i]
        m_i_idx = metrics_idx[m_i]
        o_i = operators[m_i]
        qnt *= get_metrics_operator_diff(a[m_i_idx], b[m_i_idx], o_i)
    return qnt


def main():
    csv_in = csv.DictReader(open("results_solarAltitude_1m.csv"), delimiter=";")
    cases = [row for row in csv_in]
    # header = csv_in.fieldnames

    diffs_cases = []

    mpi_max = float(max(cases, key=lambda d: float(d[_mpi]))[_mpi])
    mpcl_max = float(max(cases, key=lambda d: float(d[_mpcl]))[_mpcl])
    rep_max = float(max(cases, key=lambda d: float(d[_rep]))[_rep])
    imb_max = float(max(cases, key=lambda d: float(d[_imb]))[_imb])

    print(mpcl_max, mpi_max, rep_max, imb_max)

    global metrics
    metrics = ['mpi', 'mpcl', 'rep', 'imb']

    global metrics_idx
    metrics_idx = {'mpi': 0,
                   'mpcl': 1,
                   'rep': 2,
                   'imb':3}

    # operands = ['<<', '<', '==', '>', '>>']
    global operators
    operators = {'mpi': None,
                 'mpcl': '>>',
                 'rep': None,
                 'imb': None}

    for i1, case1 in enumerate(cases):
        if (i1 / len(cases)) % 0.1 == 0:
            print(str(i1 / len(cases)))

        # Normalized values
        mpi1 = float(case1[_mpi]) / mpi_max
        mpcl1 = float(case1[_mpcl]) / mpcl_max
        rep1 = float(case1[_rep]) / rep_max
        imb1 = float(case1[_imb]) / imb_max

        for i2 in range(i1+1, len(cases)):
            case2 = cases[i2]

            if case1['buckets'] != case2['buckets'] or \
                case1['bands'] != case2['bands'] or \
                case1['sparsity'] != case2['sparsity'] or \
                case1['bandsOffsetSeed'] != case2['bandsOffsetSeed'] or \
                case1['numPartitionsPartitioning'] != case2['numPartitionsPartitioning']:
                    continue
                # case1['rearrangementPolicy'] != case2['rearrangementPolicy'] or
                # !!!! case1['numPartitionsPartitioning'] != case2['numPartitionsPartitioning']

            # At least one of the two should
            if (case1['rearrangementPolicy'] == 'none' and case1['partitioningPolicy'] == 'MBI') or \
                    (case2['rearrangementPolicy'] == 'none' and case2['partitioningPolicy'] == 'MBI'):
                pass
            else:
                continue
            # if ((case1['rearrangementPolicy'] != 'none' and case1['partitioningPolicy'] != 'MBI') and
            #     (case2['rearrangementPolicy'] != 'none' and case2['partitioningPolicy'] != 'MBI')):


            mpi2 = float(case2[_mpi]) / mpi_max
            mpcl2 = float(case2[_mpcl]) / mpcl_max
            rep2 = float(case2[_rep]) / rep_max
            imb2 = float(case2[_imb]) / imb_max

            mpi_diff = mpi1 - mpi2
            mpcl_diff = mpcl1 - mpcl2
            rep_diff = rep1 - rep2
            imb_diff = imb1 - imb2

            if metrics[0] == 'mpi':
                diffs_cases.append((abs(mpi_diff), mpcl_diff, rep_diff, imb_diff, (i1, i2)
                    if mpi_diff >= 0 else (i2, i1)))
            elif metrics[0] == 'mpcl':
                diffs_cases.append((mpi_diff, abs(mpcl_diff), rep_diff, imb_diff, (i1, i2)
                    if mpcl_diff >= 0 else (i2, i1)))
            elif metrics[0] == 'rep':
                diffs_cases.append((mpi_diff, mpcl_diff, abs(rep_diff), imb_diff, (i1, i2)
                    if rep_diff >= 0 else (i2, i1)))
            elif metrics[0] == 'imb':
                diffs_cases.append((mpi_diff, mpcl_diff, rep_diff, abs(imb_diff), (i1, i2)
                    if imb_diff >= 0 else (i2, i1)))

    # diffs_cases_same_numparts_used = [pair for pair in diffs_cases if
    #                                      cases[pair[4][0]][_npartsu] == cases[pair[4][1]][_npartsu]]

    mpi_diffs_stddev = stdev([diff[0] for diff in diffs_cases])
    mpcl_diffs_stddev = stdev([diff[0] for diff in diffs_cases])
    rep_diffs_stddev = stdev([diff[1] for diff in diffs_cases])
    imb_diffs_stddev = stdev([diff[2] for diff in diffs_cases])

    # Selection process
    execute_stddevs = { 'mpi': mpi_diffs_stddev,
                        'mpcl': mpcl_diffs_stddev,
                        'rep': rep_diffs_stddev,
                        'imb': imb_diffs_stddev}

    cases_pairs = get_cases_given_metrics_operands(diffs_cases, execute_stddevs)
    cases_pairs = list(cases_pairs)
    # cases_pairs_sorted = sorted(list(cases_pairs), cmp=cmp_cases)
    cases_pairs.sort(key=cmp_to_key(cmp_cases))

    # Out of these results we are going to select just the best X
    selected_cases = cases_pairs[:20]

    print("final cases number: " + str(len(selected_cases)))
    print("final cases: " + str(selected_cases))

    with open("thetaJoinCases_diffs.csv", 'wt') as fout_cases :
        for pair in selected_cases:
            case1 = cases[pair[4][0]]
            case2 = cases[pair[4][1]]

            cases_pair = [case1, case2]

            for i, case in enumerate(cases_pair):
                # datasetDirectory = case[casesHeader['datasetDirectory']];
                buckets = case['buckets']

                sparsity = case['sparsity']
                bands = case['bands']
                bandsOffsetSeed = case['bandsOffsetSeed']

                rearrangementPolicy = case['rearrangementPolicy'].replace("@", "")

                partitioningPolicy = case['partitioningPolicy'].replace("@", "")
                numPartitionsPartitioning = case['numPartitionsPartitioning']
                numPartitionsUsedPartitioning = case['numPartitionsUsedPartitioning']
                searchPolicy = case['searchPolicy']
                binarySearchPolicy = case['binarySearchPolicy']

                rangeSearchUpperBoundsGranularity = case['rangeSearchUpperBoundsGranularity']
                rangeSearchWeights = case['rangeSearchWeights']

                mpi = case[_mpi]
                mpcl = case[_mpcl]
                rep = case[_rep]
                imb = case[_imb]

                params = [buckets, sparsity, bands, bandsOffsetSeed, rearrangementPolicy, numPartitionsPartitioning,
                            numPartitionsUsedPartitioning, partitioningPolicy, searchPolicy, binarySearchPolicy,
                            rangeSearchUpperBoundsGranularity, rangeSearchWeights, mpi, mpcl, rep, imb]

                fout_cases.write(",".join(params))

                fout_cases.write("\t" if i == 0 else "\n")

if __name__ == "__main__":
    main()
