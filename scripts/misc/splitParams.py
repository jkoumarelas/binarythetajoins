import sys;
import os;
import paramiko;

###########################################
# Main
def main():
	# Create the nodes dictionary object
	nodes = getNodes(); #printNodes(nodes);
	
	filePath = sys.argv[1];
	
	splitParameters(filePath, nodes);
###########################################
# Send executions
def splitParameters(filePath, nodes):
	directoryAbsolutePath = os.path.dirname(os.path.realpath(filePath));
	fileName = os.path.basename(filePath);
	fileNameWithoutExt, fileExtension = os.path.splitext(fileName);
	fileExtension = fileExtension.translate(None,'.');

	# Read file's content
	with open(filePath, 'r') as fin:
		content = fin.readlines();
		
	lines = len(content);
	linesMatchedSum = 0;

	####################################################################
	# Split file according to nodes' processing power
	for i, hostname in enumerate(sorted(nodes.keys())):
		infos = nodes[hostname];
		
		# Every node except the last
		if(i+1 < len(nodes.keys())):
			linesMatched = int(lines * float(infos['processingPowerShare']));
		else: # The last node will have to receive the remaining lines
			linesMatched = lines - linesMatchedSum;

		nodeFileName = fileNameWithoutExt + "_" + hostname + "." + fileExtension;
		
		infos['nodeFileName'] = nodeFileName;
		
		with open(directoryAbsolutePath + "/" + nodeFileName, 'w') as fout:
			for counter in range(0, linesMatched):
				fout.write(content[counter + linesMatchedSum]);
		
		linesMatchedSum += linesMatched;
		print "hostname: " + hostname + "\t" + "linesMatched: " + str(linesMatched);
########################################################################
# Print nodes
def printNodes(nodes):
	for hostname, infos in sorted(nodes.items()):
		print hostname + "\t";
		for info, value in sorted(infos.items()):
			print info + "\t" + value + "\t";
		print "\n";
########################################################################
# Get nodes object that contains information for each node.
def getNodes():
	nodes = {
	"rabbit" : {
		"ip" : "155.207.131.28",
		"cores" : "16",
		"power" : "1.0",
		"username" : "root"},
	"panther" : {
		"ip" : "155.207.131.13",
		"cores" : "8",
		"power" : "1.0",
		"username" : "root"},
	"anaconda" : {
		"ip" : "155.207.131.15",
		"cores" : "12",
		"power" : "1.0",
		"username" : "root"},
	"deer" : {
		"ip" : "155.207.131.66",
		"cores" : "8",
		"power" : "0.5",
		"username" : "root"}
	}
	totalProcessingPower = 0.0;

	# Calculate the total processing power
	for node, infos in nodes.items():
		#for info, value in infos.items():
		cores = int(infos['cores']);
		power = float(infos['power']);
		totalProcessingPower += cores * power;

	# Now calculate the processing power of each node
	for node, infos in nodes.items():
		cores = int(infos['cores']);
		power = float(infos['power']);
		processingPower = cores * power;
		processingPowerShare = processingPower / totalProcessingPower;
		
		infos['processingPower'] = str(processingPower);
		infos['processingPowerShare'] = str(processingPowerShare);
	
	return nodes;

########################################################################
########################################################################
if __name__ == "__main__":
	main();
