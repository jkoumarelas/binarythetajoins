import os;
import glob;
from os import listdir;
from os.path import isfile, join;
import shutil;

safeLock = False;

delHistograms = False;
delRealPMs = False;
delRearrangements = True;
delPartitioning = True;
delMBI = False;

datasetsBasePath = "datasets";

# [latitude, cloudLongtitude4/longtitude4, cloudSolarAltitude19/solarAltitude19,
#  cloud_seaLevelPressure/seaLevelPressure21, authNOCDayTime/dayTime]
datasetVls = ["cloudSolarAltitude19/solarAltitude19"]; # * for all

# Histograms
bucketsVls = ['100']; # * for all

# RealPMs
sparsityVls = ['1']; # * for all
bandsVls = ['1']; # * for all
bandsOffsetSeedVls = ['1']; # * for all

# Used for both rearrangements and partitioning
numReducersVls = ['*']; # * for all

# Rearrangements
#['none','BEA','BEARadius','TSPk','TSPkTransposed','TSPkTransposedNaskos'];
rearrangementPolicyVls = ['none','BEA','BEARadius','TSPk','TSPkTransposed','TSPkTransposedNaskos']; # * for all

# Partitioning
partitioningPolicyVls = ['*']; # * for all

################################################################
for dataset in datasetVls:
	datasetPath = datasetsBasePath + "/" + dataset;
	
	if delHistograms:
		for buckets in bucketsVls:
			regex = datasetPath + "/" + buckets;
			print "regex: " + regex;
			if safeLock == False:
				try:
					#shutil.rmtree(regex);
					os.system("rm -r " + regex);
				except Exception: 
					pass
	if delRealPMs:
		buckets = "*";
		for sparsity in sparsityVls:
			for bands in bandsVls:
				for bandsOffsetSeed in bandsOffsetSeedVls:
					regex = datasetPath + "/" + buckets + "/" + sparsity + "_" + bands +"_" + bandsOffsetSeed;
					
					print "regex: " + regex;
					if safeLock == False:
						try:
							#shutil.rmtree(regex);
							os.system("rm -r " + regex);
						except Exception: 
							pass
						
	if delRearrangements:
		buckets = "*";
		sparsity = "*";
		bands = "*";
		bandsOffsetSeed = "*";
		for rearrangementPolicy in rearrangementPolicyVls:
			rearrangementDirectory = rearrangementPolicy;
			for numReducers in numReducersVls:
				if rearrangementPolicy == 'none':
					continue;
				elif rearrangementPolicy == 'BEA':
					rearrangementDirectory = rearrangementPolicy;
				elif rearrangementPolicy == 'BEARadius':
					rearrangementDirectory = rearrangementPolicy + "_" + '3';
				elif rearrangementPolicy.startswith('TSPk'):
					rearrangementDirectory = rearrangementPolicy + "_" + str(numReducers);
				
				regex = datasetPath + "/" + buckets + "/" + sparsity + "_" + bands +"_" + bandsOffsetSeed + "/" + rearrangementDirectory;
				
				print "regex: " + regex;
				if safeLock == False:
					try:
						#shutil.rmtree(regex);
						os.system("rm -r " + regex);
					except Exception: 
						pass
	if delPartitioning:
		buckets = "*";
		sparsity = "*";
		bands = "*";
		bandsOffsetSeed = "*";
		for rearrangementPolicy in rearrangementPolicyVls:
			rearrangementDirectory = rearrangementPolicy;
			if rearrangementPolicy == 'none':
				rearrangementDirectory = "";
			elif rearrangementPolicy == '*':
				rearrangementDirectory = "*" + "/";
			elif rearrangementPolicy == 'BEA':
				rearrangementDirectory = rearrangementPolicy + "/";
			elif rearrangementPolicy == 'BEARadius':
				rearrangementDirectory = rearrangementPolicy + "_" + '3' + "/";
			elif rearrangementPolicy.startswith('TSPk'):
				rearrangementDirectory = rearrangementPolicy + "_" + str(numReducers) + "/";
			for partitioningPolicy in partitioningPolicyVls:
				for numReducers in numReducersVls:
					regex = datasetPath + "/" + buckets + "/" + sparsity + "_" + bands +"_" + bandsOffsetSeed + "/" + rearrangementDirectory + partitioningPolicy + "_" + str(numReducers);
					
					print "regex: " + regex;
					if safeLock == False:
						try:
							#shutil.rmtree(regex);
							os.system("rm -r " + regex);
						except Exception: 
							pass
	if delMBI:
		buckets = "*";
		sparsity = "*";
		bands = "*";
		bandsOffsetSeed = "*";
		rearrangementPolicy = "*";
		partitioningPolicy = "*";
		numReducers = "*";
		
		regex = datasetPath + "/" + buckets + "/" + sparsity + "_" + bands +"_" + bandsOffsetSeed + "/" + rearrangementPolicy + "/" + partitioningPolicy + "_" + numReducers + "/" + "MBucketI";
		
		print "regex: " + regex;
		if safeLock == False:
			try:
				#shutil.rmtree(regex);
				os.system("rm -r " + regex);
			except Exception: 
				pass
