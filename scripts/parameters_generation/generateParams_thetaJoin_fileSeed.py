#!/usr/bin/python
import os;
import shutil;
import sys;
import string;

def main():

	datasetsFolder = "datasets";
	
	dataset = "solarAltitude";

	#executable path for TSPk. btj directory already exists to the same path with the root directory "datasets"
	tspkPath = "btj/tspk";

	#the folder which contains the generated .txt parameters file
	parametersFolder = "parameters"

	#Overrides the default hadoop parameter which is 10minutes
	jobMaxExecutionHours = '30'; # 30 hours

	thetaJoinCasesFilePath = sys.argv[1];

	cases = open(thetaJoinCasesFilePath,'r').readlines();

	mBucketIParametersFolder = parametersFolder + "/" + dataset;
	if not os.path.exists(mBucketIParametersFolder):
		os.makedirs(mBucketIParametersFolder);
	mBucketIParametersFile = mBucketIParametersFolder + "/" + "mBucketI.txt";

	foutMBI = open(mBucketIParametersFile,'w');

	# Path to the actual datatset
	datasetDirectory = datasetsFolder+"/"+dataset;

	for case in cases:
		#case = case.encode('utf-8');
		case = string.replace(case, '\n', '' );
		print(case);
		
		tokens = case.split(',');
		
		buckets = int(tokens[0]);
		
		sparsity = int(tokens[1]);
		bands = int(tokens[2]);
		bandsOffsetSeed = int(tokens[3]);
		
		rearrangementPolicy = tokens[4];
		numPartitions = int(tokens[5]);
		
		partitioningPolicy = tokens[6];
		maxPartitionInputPolicy = tokens[7];
		
		rangeSearchUpperBoundsGranularity = tokens[8];
		rangeSearchWeights = tokens[9];
		
		partitionMatrixDirectory = datasetDirectory + "/" + str(buckets) + "/" + str(sparsity) + "_" + str(bands) + "_" + str(bandsOffsetSeed);
		
		createParameters_MBI(buckets,rearrangementPolicy,datasetDirectory, partitioningPolicy, maxPartitionInputPolicy, rangeSearchUpperBoundsGranularity, rangeSearchWeights,numPartitions,jobMaxExecutionHours, partitionMatrixDirectory,foutMBI);
		
	exit(0);

########################################################################
# Partitioning - Parameters generation
def createParameters_MBI(buckets,rearrangementPolicy,datasetDirectory, partitioningPolicy,maxPartitionInputPolicy, rangeSearchUpperBoundsGranularity, rangeSearchWeights,numPartitions,jobMaxExecutionHours, partitionMatrixDirectory,foutMBI):
	"This method creates the parameters for the Partitioning execution mode in a form that is accepted by the system"
	if rearrangementPolicy == 'none':
		partitionMatrixRearrangedDirectory = partitionMatrixDirectory;
	elif rearrangementPolicy == 'BEA':
		partitionMatrixRearrangedDirectory = partitionMatrixDirectory + "/" + rearrangementPolicy;
	elif rearrangementPolicy == 'BEARadius':
		partitionMatrixRearrangedDirectory = partitionMatrixDirectory + "/" + rearrangementPolicy + "_" + '3';
	elif rearrangementPolicy.startswith('TSPk'):
		partitionMatrixRearrangedDirectory = partitionMatrixDirectory + "/" + rearrangementPolicy + "_" + str(numPartitions);
	
	#print(partitionMatrixRearrangedDirectory  + "\n");
	
	if not (os.path.exists(partitionMatrixRearrangedDirectory)):
		return;

	partitioningDirectory = partitionMatrixRearrangedDirectory + "/" + partitioningPolicy + "_" + str(numPartitions) + "/" + maxPartitionInputPolicy;
	if(maxPartitionInputPolicy == 'RANGE_SEARCH'):
		partitioningDirectory = partitioningDirectory + "_" + rangeSearchUpperBoundsGranularity + "/" + rangeSearchWeights;

	params = "executionMode=MBucketI";
	
	params += "\t" + "buckets=" + str(buckets);
	
	params += "\t" + "numPartitions=" + str(numPartitions);
	params += "\t" + "jobMaxExecutionHours=" + str(jobMaxExecutionHours);
	params += "\t" + "datasetDirectory=" + datasetDirectory;
	
	params += "\t" + "boundaries=" + datasetDirectory + "/" + str(buckets) + "/" + "boundaries.csv";
	params += "\t" + "counts=" + datasetDirectory + "/" + str(buckets) + "/" + "counts.csv";
	
	if rearrangementPolicy != 'none':
		params += "\t" + "rearrangements=" + partitionMatrixRearrangedDirectory + "/" + "rearrangements.csv";
	
	params += "\t" + "properties=" + partitionMatrixRearrangedDirectory + "/" + "properties.csv";
	params += "\t" + "histogramIndexToPartitionsMapping=" + partitioningDirectory + "/" + "histogramIndexToPartitionsMapping.csv";
	params += "\t" + "partitionToCellsMapping=" + partitioningDirectory + "/" + "partitionToCellsMapping.csv";
	params += "\t" + "mBucketIDirectory=" + partitioningDirectory + "/" + "MBucketI";
	
	foutMBI.write(params + "\n");

if __name__ == "__main__":
	main();
