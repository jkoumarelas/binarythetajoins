########################################################################
#
#    generateParametersBTJCompact.py
#
#    The goal of this script is to provide an easy way to generate parameters
#    for the Binary Theta Joins project.
#
#    The user has to provide parameters at the part 1 and at the parts 2.1
#    and 2.2 the systems generates the parameters.
#
#    Author: John Koumarelas
#
########################################################################
########################################################################
########################################################################
#!/usr/bin/python
import os
import shutil
from collections import defaultdict
#~ from parameters_generation.generate_params_real import *
from generate_params_real import *

########################################################################
########################################################################
#
#    Part 1
#
#    In this part, we specify the parameters to be generated. More specifically
#    we specify lists of parameters and a cross-product of those gets generated in the end.
#
########################################################################

def sort_normalize_weights(weights):
    new_weights = []
    for weights_tok in weights:
        new_weights_tok = ""
        sum_weights = 0.0
        weights_case = []
        
        weight_toks = weights_tok.split("-")
        for weight_tok in weight_toks:
            weights_case.append(weight_tok)
            sum_weights += float(weight_tok.split("_")[1])
        
        sorted_weights_case = sorted(weights_case)
        for i in range(0,len(sorted_weights_case)):
            name = sorted_weights_case[i].split('_')[0]
            weight = float(sorted_weights_case[i].split('_')[1])
            new_weights_tok += name + "_" + str(weight/sum_weights) + "-"
        new_weights_tok = new_weights_tok[:-1]
        
        new_weights.append(new_weights_tok)
    return new_weights


os.chdir('../../')

matrix_generation = 'random' # 'random' # Creation of Partition Matrices (from real data). NOT virtual...
#~ matrix_generation = 'real' # 'random' # Creation of Partition Matrices (from real data). NOT virtual...

datasetsFolder = "datasets"
dataset = "solarAltitude_1m"

# Executable path for TSPk. btj directory already exists to the same path with the root directory "datasets"
tspkPath = "btj/tspk"

#Overrides the default hadoop parameter which is 10minutes
jobMaxExecutionHours = '30' # 30 hours

#~ buckets_vls = [100, 200]#, 100, 400]  #the number of buckets used. We only use 100 buckets in our experiments
buckets_vls = [100, 200]#, 100, 400]  #the number of buckets used. We only use 100 buckets in our experiments

# The width of the band depends on the skewness of the dataset. bucket[i+sparsity]-bucket[i].
# 1 is the index so we take into consideration neighbour buckets.


rearrangementPolicy_vls = ['none','BEA','BEARadius','TSPk','TSPkTransposedOnDefault']
rearrangementPolicyDefault = 'none'
numPartitions_vls = [10,20,40,80]  # The number of partitions (e.g. reducers, executors)

#~ partitioningPolicy_vls = ['AICPM','AICPS', 'ACCPM', 'EPIC', 'EPCC','M','WICRC_1.0_1.0','WICRC_1.2_1.0','WICRC_1.5_1.0']
partitioningPolicy_vls = ['MBI']#['AICPS']
partitioningPolicyDefault = 'MBI'

searchPolicy_vls = ['BINARY_SEARCH']  #['RANGE_SEARCH','BINARY_SEARCH']
searchPolicyDefault = 'BINARY_SEARCH'

# For BINARY_SEARCH only : ['MAX_PARTITION_INPUT', 'MAX_PARTITION_CANDIDATE_CELLS']
binarySearchPolicy_vls = ['MAX_PARTITION_INPUT', 'MAX_PARTITION_CANDIDATE_CELLS']
binarySearchPolicyDefault = 'MAX_PARTITION_INPUT'

# For RANGE_SEARCH only
rangeSearchPartitioningPolicyDefault = 'SAME'  # 'SAME' or other ( 'MBI', 'MBIREP' etc.)
rangeSearchUpperBoundGranularity = '2.0_11'
# {max,min,mean,median,stdev,rep,imb}_{IC,CC}
# e.g. 'maxIC=0.3_imbIC=0.7-maxIC=0.5_maxCC=0.5'   2 sets of weights.
rangeSearchWeights_vls = ['BJrepIC_1.0-BJmaxIC_0.0-BJmaxCC_0.0',
                          'BJrepIC_0.0-BJmaxIC_1.0-BJmaxCC_0.0',
                          'BJrepIC_0.0-BJmaxIC_0.0-BJmaxCC_1.0',
                          'BJrepIC_0.5-BJmaxIC_0.0-BJmaxCC_0.5',
                          'BJrepIC_0.33-BJmaxIC_0.33-BJmaxCC_0.33',
                          'BJrepIC_0.25-BJmaxIC_0.25-BJmaxCC_0.5']
sortedRangeSearchWeights_vls = sort_normalize_weights(rangeSearchWeights_vls) # To avoid duplications on PATHs and normalize weights (sum == 1.0).
########################################################################

## Part 3.1. File creation and handlers. ##

fout_matrix_generation, fout_rearrangements, fout_partitioning = None, None, None

if matrix_generation == 'real':
    dataset_parameters_dir = 'parameters' + '/' + dataset
    if not os.path.exists(dataset_parameters_dir):
        os.makedirs(dataset_parameters_dir)

    fout_matrix_generation = open(dataset_parameters_dir + '/' + "partitionMatrix.txt",'wt')
    fout_rearrangements = open(dataset_parameters_dir + '/' + "rearrangements.txt",'wt')
    fout_partitioning = open(dataset_parameters_dir + '/' + "partitioning.txt",'wt')

    # Path to the actual datatset
    datasetDirectory = datasetsFolder + '/' + dataset

    bands_vls = range(1, 7)  # The number of the bands 1-8
    sparsity_bands_vls = [1]
    bandsOffsetSeed_vls = range(0, 10)  # For every band value (bandsVls) there are 100 different band formations.

    for searchPolicy in searchPolicy_vls:
        for buckets in buckets_vls:
            histogramsDirectory= datasetDirectory + "/" + str(buckets)
            for sparsity_bands in sparsity_bands_vls:
                for bands in bands_vls:
                    for bandsOffsetSeed in bandsOffsetSeed_vls:
                        partitionMatrixDirectory = histogramsDirectory + "/" + str(sparsity_bands) + "_" + str(bands) + "_" + str(bandsOffsetSeed)

                        createParameters_matrix_factory_real(buckets, sparsity_bands, bands, bandsOffsetSeed, datasetDirectory, histogramsDirectory, fout_matrix_generation)

                        for rearrangementPolicy in rearrangementPolicy_vls:
                            partitionMatrixDirectory = datasetDirectory + "/" + str(buckets) + "/" + str(sparsity_bands) + "_" + str(bands) + "_" + str(bandsOffsetSeed)

                            #############################
                            createParameters_rearrangements(datasetDirectory, rearrangementPolicy, partitionMatrixDirectory, numPartitions_vls, tspkPath, fout_rearrangements)

                            for partitioningPolicy in partitioningPolicy_vls:
                                for numPartitions in numPartitions_vls:
                                    for rangeSearchWeights in sortedRangeSearchWeights_vls:
                                        #############################
                                        # Partitioning & MBucketI    #
                                        for binarySearchPolicy in binarySearchPolicy_vls:
                                            if partitioningPolicy.startswith("MBI") and  binarySearchPolicy != "MAX_PARTITION_INPUT":
                                                continue
                                            if searchPolicy == "BINARY_SEARCH":
                                                createParameters_partitioning(datasetDirectory, rearrangementPolicy,partitioningPolicy, searchPolicy, binarySearchPolicy,
                                                    binarySearchPolicyDefault, "None",['None'], numPartitions, partitionMatrixDirectory, fout_partitioning)
                                            elif searchPolicy == "RANGE_SEARCH":
                                                createParameters_partitioning(datasetDirectory, rearrangementPolicy,partitioningPolicy, searchPolicy, binarySearchPolicy,
                                                    binarySearchPolicyDefault, rangeSearchUpperBoundGranularity, rangeSearchWeights, numPartitions, partitionMatrixDirectory, fout_partitioning)
elif matrix_generation == 'random':
    dataset_parameters_dir = 'parameters' + '/' + 'random'
    if not os.path.exists(dataset_parameters_dir):
        os.makedirs(dataset_parameters_dir)

    fout_matrix_generation = open(dataset_parameters_dir + '/' + "partitionMatrix.txt", 'wt')
    fout_rearrangements = open(dataset_parameters_dir + '/' + "rearrangements.txt", 'wt')
    fout_partitioning = open(dataset_parameters_dir + '/' + "partitioning.txt", 'wt')

    # Path to the actual datatset
    dataset_dir = dataset_parameters_dir

    sparsity_vls = [0.01]
    seed_vls = range(0, 50)  # For every band value (bandsVls) there are 100 different band formations.
    #~ seed_vls = range(23, 50)  # For every band value (bandsVls) there are 100 different band formations.

    for searchPolicy in searchPolicy_vls:
        for buckets in buckets_vls:
            buckets_dir = dataset_dir + '/' + str(buckets)
            for sparsity in sparsity_vls:
                for seed in seed_vls:
                    partitionMatrixDirectory = buckets_dir + "/" + str(sparsity) + "_" + str(seed)

                    createParameters_matrix_factory_random(buckets, sparsity, seed, dataset_dir, fout_matrix_generation)

                    for rearrangementPolicy in rearrangementPolicy_vls:
                        partitionMatrixDirectory = dataset_dir + "/" + str(buckets) + "/" + str(sparsity) + "_" + str(seed)

                        #############################
                        createParameters_rearrangements(dataset_dir, rearrangementPolicy, partitionMatrixDirectory,
                                                        numPartitions_vls, tspkPath, fout_rearrangements)

                        for partitioningPolicy in partitioningPolicy_vls:
                            for numPartitions in numPartitions_vls:
                                for rangeSearchWeights in sortedRangeSearchWeights_vls:
                                    #############################
                                    # Partitioning & MBucketI    #
                                    for binarySearchPolicy in binarySearchPolicy_vls:
                                        if partitioningPolicy.startswith(
                                                "MBI") and binarySearchPolicy != "MAX_PARTITION_INPUT":
                                            continue
                                        if searchPolicy == "BINARY_SEARCH":
                                            createParameters_partitioning(dataset_dir, rearrangementPolicy,
                                                                          partitioningPolicy, searchPolicy,
                                                                          binarySearchPolicy,
                                                                          binarySearchPolicyDefault, "None",
                                                                          ['None'], numPartitions,
                                                                          partitionMatrixDirectory,
                                                                          fout_partitioning)
                                        elif searchPolicy == "RANGE_SEARCH":
                                            createParameters_partitioning(dataset_dir, rearrangementPolicy,
                                                                          partitioningPolicy, searchPolicy,
                                                                          binarySearchPolicy,
                                                                          binarySearchPolicyDefault,
                                                                          rangeSearchUpperBoundGranularity,
                                                                          rangeSearchWeights, numPartitions,
                                                                          partitionMatrixDirectory,
                                                                          fout_partitioning)

fout_matrix_generation.close()
fout_rearrangements.close()
fout_partitioning.close()
