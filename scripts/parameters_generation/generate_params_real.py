import os
from collections import defaultdict


# Real Partition Matrix - Parameters generation
def createParameters_matrix_factory_real(buckets, sparsity, bands, bandsOffsetSeed, datasetDirectory, histogramsDirectory, fout):
    "This method creates the parameters for the Real Partition Matrix execution mode in a form that is accepted by the system"

    prms = defaultdict()
    prms['executionMode'] = 'realPartitionMatrix'

    prms['buckets'] = buckets
    prms['sparsity'] = sparsity
    prms['bands'] = bands
    prms['bandsOffsetSeed'] = bandsOffsetSeed

    prms['datasetDirectory'] = datasetDirectory
    prms['histogramsDirectory'] = histogramsDirectory

    partitionMatrixDirectory = datasetDirectory + "/" + str(buckets) + "/" + str(sparsity) + "_" + str(bands) + "_" + \
                               str(bandsOffsetSeed)
    prms['partitionMatrixDirectory'] = partitionMatrixDirectory

    fout.write("\t".join([x + "=" + str(prms[x]) for x in sorted(prms.keys())]) + "\n")

# Random Partition Matrix - Parameters generation
def createParameters_matrix_factory_random(buckets, sparsity, seed, datasetDirectory, fout):
    "This method creates the parameters for the Real Partition Matrix execution mode in a form that is accepted by the system"

    prms = defaultdict()
    prms['executionMode'] = 'randomPartitionMatrix'

    prms['buckets'] = buckets
    prms['sparsity'] = sparsity
    prms['seed'] = seed

    prms['datasetDirectory'] = datasetDirectory

    partitionMatrixDirectory = datasetDirectory + "/" + str(buckets) + "/" + str(sparsity) + "_" + str(seed)
    prms['partitionMatrixDirectory'] = partitionMatrixDirectory

    fout.write("\t".join([x + "=" + str(prms[x]) for x in sorted(prms.keys())]) + "\n")

####

def get_numPartitions_of_directory(dirpath):
    with open(dirpath + '/' + 'partitionsInputCost.csv', 'rt') as fin:
        lines = fin.readlines()
        return len(lines)

# Rearrangements - Parameters generation
def createParameters_rearrangements(datasetDirectory, rearrangementPolicy, partitionMatrixDirectory, numPartitionsVls, tspkPath, fout):
    "This method creates the parameters for the Rearrangements execution mode in a form that is accepted by the system"

    if not (os.path.exists(partitionMatrixDirectory)):
        return

    if rearrangementPolicy == "none":
        return

    # Input
    prms = defaultdict()
    prms['executionMode'] = 'rearrangement'
    prms['rearrangementPolicy'] = rearrangementPolicy
    prms['partitionMatrixDirectory'] = partitionMatrixDirectory
    prms['datasetDirectory'] = datasetDirectory

    if rearrangementPolicy == 'none':
        prms['numPartitions'] = '-1'
        
        fout.write("\t".join([x + "=" + str(prms[x]) for x in sorted(prms.keys())]) + "\n")
    elif rearrangementPolicy == 'BEA':
        partitionMatrixRearrangedDirectory = partitionMatrixDirectory + "/" + rearrangementPolicy
        prms['partitionMatrixRearrangedDirectory'] = partitionMatrixRearrangedDirectory
        prms['numPartitions'] = '-1'
        
        fout.write("\t".join([x + "=" + str(prms[x]) for x in sorted(prms.keys())]) + "\n")
    elif rearrangementPolicy == 'BEARadius':
        partitionMatrixRearrangedDirectory = partitionMatrixDirectory + "/" + rearrangementPolicy + "_" + "3"

        prms['partitionMatrixRearrangedDirectory'] = partitionMatrixRearrangedDirectory
        prms['numPartitions'] = '-1'
        
        fout.write("\t".join([x + "=" + str(prms[x]) for x in sorted(prms.keys())]) + "\n")
    elif rearrangementPolicy.startswith('TSP'):
        for numPartitions in numPartitionsVls:
            partitionMatrixRearrangedDirectory = partitionMatrixDirectory + "/" + rearrangementPolicy + "_" + str(
                numPartitions)

            prms['numPartitions'] = numPartitions
            prms['partitionMatrixRearrangedDirectory'] = partitionMatrixRearrangedDirectory
            prms['tspk'] = tspkPath

            fout.write("\t".join([x + "=" + str(prms[x]) for x in sorted(prms.keys())]) + "\n")

###

# Partitioning - Parameters generation
def createParameters_partitioning(datasetDirectory, rearrangementPolicy, partitioningPolicy, searchPolicy,
                           binarySearchPolicy, binarySearchPolicyDefault,
                           rangeSearchUpperBoundGranularity, rangeSearchWeights,  # sortedRangeSearchWeightsVls,
                           numPartitions, partitionMatrixDirectory, fout):
    "This method creates the parameters for the Partitioning execution mode in a form that is accepted by the system"

    partitionMatrixRearrangedDirectory = ''
    if rearrangementPolicy == 'none':
        partitionMatrixRearrangedDirectory = partitionMatrixDirectory
    elif rearrangementPolicy == 'BEA':
        partitionMatrixRearrangedDirectory = partitionMatrixDirectory + "/" + rearrangementPolicy
    elif rearrangementPolicy == 'BEARadius':
        partitionMatrixRearrangedDirectory = partitionMatrixDirectory + "/" + rearrangementPolicy + "_" + '3'
    elif rearrangementPolicy.startswith('TSPk'):
        partitionMatrixRearrangedDirectory = partitionMatrixDirectory + "/" + rearrangementPolicy + "_" + str(
            numPartitions)

    if not (os.path.exists(partitionMatrixRearrangedDirectory)):
        return

    prms = defaultdict()
    prms['executionMode'] = 'partitioning'
    prms['partitioningPolicy'] = partitioningPolicy
    prms['partitionMatrixDirectory'] = partitionMatrixRearrangedDirectory
    prms['searchPolicy'] = searchPolicy
    prms['binarySearchPolicy'] = binarySearchPolicy
    prms['datasetDirectory'] = datasetDirectory

    if rearrangementPolicy != 'none':
        prms['rearrangements'] = partitionMatrixRearrangedDirectory + "/" + "rearrangements.csv"

    # num partitions
    prms['numPartitions'] = str(numPartitions)

    defaultMbiPartitioningDirectory = partitionMatrixRearrangedDirectory + "/" + "MBI" + "_" + str(
        numPartitions) + "_" + 'actual' + "/" + "BINARY_SEARCH" + "-" + binarySearchPolicyDefault

    if searchPolicy == 'RANGE_SEARCH':
        prms['rangeSearchUpperBoundGranularity'] = rangeSearchUpperBoundGranularity
        prms['rangeSearchWeights'] = rangeSearchWeights  # "|".join(sortedRangeSearchWeightsVls)

    partitioningDirectory = partitionMatrixRearrangedDirectory + "/" + partitioningPolicy + "_" + str(numPartitions) + \
                            "/" + searchPolicy + "-" + binarySearchPolicy
    if searchPolicy == 'BINARY_SEARCH':
        prms['partitioningDirectory'] = partitioningDirectory
    elif searchPolicy == 'RANGE_SEARCH':
        prms['partitioningDirectory'] = partitioningDirectory + "-" + rangeSearchUpperBoundGranularity

        defaultPartitioningDirectory = partitionMatrixRearrangedDirectory + "/" + partitioningPolicy + "_" + str(
            numPartitions) + "/" + "BINARY_SEARCH" + "-" + binarySearchPolicy
        if not (os.path.exists(defaultPartitioningDirectory)):
            print("No defaultPartitioningDirectory: " + defaultPartitioningDirectory + " does not exist")
            return

        prms['defaultPartitioningDirectory'] = defaultPartitioningDirectory

    fout.write("\t".join([x + "=" + str(prms[x]) for x in sorted(prms.keys())]) + "\n")
