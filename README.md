Master's Thesis project.

This project repository, implements the algorithms described in the 'Flexible Partitioning for Binary Theta-Joins in a Massively Parallel Setting'. 
These algorithms rearrange and partition a Join Matrix, with different techniques, with the goal of improving certain metrics, such as rep (Replication Factor), imb (Imbalance across nodes), mri (Max Reducer Input).
This effect is directly correlated with the overall execution time, as explained in the paper.

* The three Ant scripts in the root folder, help the building and execution of the project.
1. build_btj_project.xml --> Should be called first to compile the *.java into *.class files.
2. For single-thread executions --> build_btj_controller.jar.xml, whereas for multi-threaded, that implements the Producer-Consumer model, please use the build_btj_producer.jar.xml.

* Scripts can be found for various uses, such as for generating parameters for your experiments, in the sources folder.

* The execution is initiated from the control/Controller.java class.

For the accompanying tools for collecting metadata and generating statistics, please contact me.