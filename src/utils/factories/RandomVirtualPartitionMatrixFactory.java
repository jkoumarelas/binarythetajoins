/**
 * VirtualPartitionMatrixFactory.java
 * 
 * This class is deprecated. Use of it is not recommended.
 * (Modify at your own will of course!)
 * 
 * It is supposed to allow the creation of AD-HOC
 * PartitionMatrices, that does not rely on a specific
 * Dataset to be created, and test mostly visual differences.
 * 
 * @author John Koumarelas
 */

package utils.factories;

import java.util.Arrays;
import java.util.Random;

import model.BucketBoundaries;
import model.PartitionMatrix;
import datatypes.IntPair;

public class RandomVirtualPartitionMatrixFactory {
	
	private int buckets;
	private int bandsOffsetSeed;
	private double sparsity;
	
	private long sizeS;
	private long sizeT;
	
	private BucketBoundaries[] boundariesS;
	private BucketBoundaries[] boundariesT;
	private long[] countsS;
	private long[] countsT;
	private long[][] matrix;

	private PartitionMatrix pm;
	
	private long executionTime;

	public RandomVirtualPartitionMatrixFactory(int buckets, int bandsOffsetSeed, double sparsity) {
		this.buckets = buckets;
		this.bandsOffsetSeed = bandsOffsetSeed;
		this.sparsity = sparsity;
		
		boundariesS = new BucketBoundaries[buckets];
		boundariesT = new BucketBoundaries[buckets];
		
		for(int i = 0 ; i < buckets; ++i) {
			boundariesS[i] = new BucketBoundaries();
			boundariesT[i] = new BucketBoundaries();
		}
		
		countsS = new long[buckets];
		countsT = new long[buckets];
		matrix = new long[buckets][buckets];

		for(int i = 0 ; i < buckets; ++i) {
			Arrays.fill(matrix[i],0);
		}
	}

	public PartitionMatrix getPartitionMatrix() {
		long start = System.currentTimeMillis();
		
		PartitionMatrix pm = null;
		pm = caseRandomPM(this.sparsity);
		long end = System.currentTimeMillis();
		executionTime = end-start;
		return pm;
	}
	
	void setBoundaries(){
		for(int i = 0 ; i < buckets; ++i) {
			boundariesS[i].set(i, i+1);
			boundariesT[i].set(i, i+1);
		}
	}
	
	void calculateCountAndSize() {
		/*	XXX: Calculate countsS and countsT differently.	*/
		/* 		   	this implementation uses the same cell value two times,	*/
		/* 			one for the row and one for the column	*/
		
		for(int i = 0 ; i < buckets; ++i){
			countsS[i]=0;
			for(int j = 0 ; j < buckets; ++j) {
				if(matrix[i][j]>0) {
					countsS[i] = 1;
					break;
				}
			}
		}
		
		for(int j = 0 ; j < buckets; ++j){
			countsT[j]=0;
			for(int i = 0 ; i < buckets; ++i) {
				if(matrix[i][j]>0) {
					countsT[j] = 1;
					break;
				}
			}
		}
		
		sizeS = buckets;
		sizeT = buckets;
	}
	
	void setFieldsPartitionMatrix() {
		pm = new PartitionMatrix();
		
		pm.setMatrix(matrix);
		pm.setBoundariesS(boundariesS);
		pm.setBoundariesT(boundariesT);
		pm.setCountsS(countsS);
		pm.setCountsT(countsT);
		pm.setSizeS(sizeS);
		pm.setSizeT(sizeT);
	}

	public PartitionMatrix caseRandomPM(double sparsity) {
		Random random = new Random(bandsOffsetSeed);
	
		for(int i = 0; i < buckets; ++i) {
			for(int j = 0; j < buckets; ++j) {
				if (random.nextDouble() <= this.sparsity) {
//				if (random.nextGaussian() <= this.sparsity) {
					matrix[i][j] = 2;
				}
			}
		}
		
		setBoundaries(); // set some default boundariesS and boundariesT.
		calculateCountAndSize(); // FIXME: check how count and size are calculated.
		setFieldsPartitionMatrix(); // create PartitionMatrix and set fields.
	
		return pm;
	}
	
	public long getExecutionTime() {
		return executionTime;
	}
	
	public static void main(String[] args) {
		int buckets = 10;
		int seed = 0;
		double sparsity = 0.25;
		
		RandomVirtualPartitionMatrixFactory fctr = new RandomVirtualPartitionMatrixFactory(buckets, seed, sparsity);
		PartitionMatrix pm = fctr.getPartitionMatrix();
		int counter = 0;
		for(int i = 0 ; i < buckets; ++i) {
			System.out.println("");
			for(int j = 0; j < buckets; ++j) {
				System.out.print(fctr.matrix[i][j] + ",");
				if(fctr.matrix[i][j] > 0) {
					++counter;
				}
			}
		}
		
		System.out.println("\n" + (counter / ((double)buckets*buckets)));
	}
	
}
